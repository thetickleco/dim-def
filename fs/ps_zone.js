load('ps_timer.js');
load('api_pwm.js');
load('ps_daystarted.js');

print("Inside PS_ZoNE");
let pwm_pin = {
  "CHANNEL_1": 2,
  "CHANNEL_2": 4,
  "CHANNEL_3": 16,
  "CHANNEL_4": 17,
  "CHANNEL_5": 5,
  "CHANNEL_6": 18
};
let ZONE = {

    checkUnderWhichZone: function (zonedetails) {
        let currentTime = CUST_TIMER.now();

        for(let i=0; i<zonedetails.PSLDWT.length - 1; i++) {
            if(currentTime >= zonedetails.START_DAY_TIME ){
                if(currentTime <= zonedetails.PSLDWT[i].END_ZONE_TIME) {
                    return i;
                }
            }
        }

        return -1;

    },

    startZone: function(zoneNo, zonedetails) {
        let zone =  zonedetails.PSLDWT[zoneNo];


        PWM.set(pwm_pin.CHANNEL_1, 10000, zone.CH_INTEN_1 /100 );
        PWM.set(pwm_pin.CHANNEL_2, 10000, zone.CH_INTEN_2 /100 );
        PWM.set(pwm_pin.CHANNEL_3, 10000, zone.CH_INTEN_3 /100 );
        PWM.set(pwm_pin.CHANNEL_4, 10000, zone.CH_INTEN_4 /100 );
        PWM.set(pwm_pin.CHANNEL_5, 10000, zone.CH_INTEN_5 /100 );
        PWM.set(pwm_pin.CHANNEL_6, 10000, zone.CH_INTEN_6 /100 );

        CUST_TIMER.set(zonedetails.PSLDWT[zoneNo].END_ZONE_TIME - CUST_TIMER.now(), 0, function () {
            if(zoneNo === (zonedetails.PSLDWT.length - 1)) {
                DAYSTARTED.check(zonedetails);
            } else {
                this.startZone(zoneNo + 1, zonedetails);
            }
        }, null );
    }

};