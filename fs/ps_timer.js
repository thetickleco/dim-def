load('api_timer.js');
load('api_math.js');

print("Inside PS_TIMER");

let CUST_TIMER = {

    now: function () {
        let now = Timer.now();
        let flr;
        print("TIME NOW In PS Timer:"+ JSON.stringify(now));
        // its check the controller if it was started more the 24 hour ago
        // it will give you the time in second's from 12am of same day.
        flr = (now > 86400 )?(Math.floor(now))%86400:Math.floor(now);
        return flr;
    },

    set: function (milliseconds, flags, handler, userdata) {
        return Timer.set(milliseconds, flags, handler, userdata);
    },

    del: function (id) {
        Timer.del(id);
    }
};
