load('ps_timer.js');
load('api_file.js');
load('api_log.js');
load('ps_zone.js');

print("Inside PS_DAYSTARTED");
let DAYSTARTED = {
    check: function (zonedetails) {
        let currentTime = CUST_TIMER.now();
        print("CURRENt tIME : "+JSON.stringify(currentTime));
        if(currentTime >= zonedetails.START_DAY_TIME && currentTime <= zonedetails.PSLDWT[zonedetails.PSLDWT.length -1].END_ZONE_TIME) {
            Log.debug("/CHECK/FOUND/INSIDE/ZONE");
            this.sendToZone(zonedetails);
        }else if(currentTime >= zonedetails.PSLDWT[zonedetails.PSLDWT.length -1].END_ZONE_TIME){
            Log.debug("/CHECK/FOUND/AFTER/ZONE");
            this.afterZone(zonedetails);
        } else {
            Log.debug("/CHECK/FOUND/BEFORE/ZONE");
            this.beforeZone(zonedetails);
        }
        
    },
    
    beforeZone: function (zonedetails) {
        Log.debug("/START/TIMER/TILL/FIRST/ZONE");
        let currentTime = CUST_TIMER.now();
        // time to start - tts
        let tts = zonedetails.START_DAY_TIME - currentTime;
        //this.timer(tts);

        // it is a timer. it ends up with starting first zone.
        CUST_TIMER.set(tts * 1000, 0, function() {
            this.sendToZone(zonedetails);
        }, null);
    },

    // compute after zone complete
    afterZone: function (zonedetails) {
        Log.debug("/START/TIMER/TILL/FIRST/ZONE");
        let currentTime = CUST_TIMER.now();
        // time to change day - tts
        let ttcd = 86400 - currentTime;
        //this.timer(ttcd);

        // it is a timer. it ends up with starting first zone.
        CUST_TIMER.set(ttcd * 1000, 0, function() {
            this.check(zonedetails);
        }, null);
    },
    
    sendToZone : function (zonedetails) {
        Log.debug("/INSIDE/ZONE");
        let zoneNo = ZONE.checkUnderWhichZone(zonedetails);
        if(zoneNo === -1) {
            this.check(zonedetails);
        } else {
            ZONE.startZone(zoneNo,zonedetails);
        }

    }
};