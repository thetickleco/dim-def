#include "mgos.h"

enum mgos_app_init_result mgos_app_init(void) {

    struct timezone tz = {9, 0};
    int _rtc_flag = 1;
    LOG(LL_INFO, ("Time: %lf", mg_time()));
    if(mgos_settimeofday(86460, &tz) == 0 ) {
        LOG(LL_INFO, ("OK" ));
    } else {
        LOG(LL_INFO, ("NOK" ));
    }

    return MGOS_APP_INIT_SUCCESS;


}


